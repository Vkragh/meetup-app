import getCollection from "../../utils/getCollection";

async function handler(req, res) {
    if (req.method === "POST") {
        const data = req.body;
        const { client, collection: meetupsCollection } = await getCollection(
            "meetups"
        );
        const result = await meetupsCollection.insertOne(data);

        console.log(result);

        client.close();

        res.status(201).json({ message: "Meetup inserted" });
    }
}

export default handler;
