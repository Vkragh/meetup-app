import Head from "next/head";
import getCollection from "../utils/getCollection";
import MeetupList from "../components/meetups/MeetupList";
import { Fragment } from "react";

function HomePage({ meetups }) {
    return (
        <Fragment>
            <Head>
                <title>Meetups</title>
            </Head>
            <MeetupList meetups={meetups} />
        </Fragment>
    );
}

export async function getStaticProps() {
    const { client, collection: meetupsCollection } = await getCollection(
        "meetups"
    );

    const meetups = await meetupsCollection.find().toArray();

    client.close();

    return {
        props: {
            meetups: meetups.map((meetup) => ({
                title: meetup.title,
                address: meetup.address,
                image: meetup.image,
                id: meetup._id.toString(),
            })),
        },
        revalidate: 10,
    };
}

export default HomePage;
