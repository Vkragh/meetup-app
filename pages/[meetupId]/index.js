import { ObjectId } from "mongodb";
import MeetupDetail from "../../components/meetups/MeetupDetail";
import getCollection from "../../utils/getCollection";

function MeetupDetails({ meetupData: { image, title, address, description } }) {
    return (
        <MeetupDetail
            image={image}
            title={title}
            address={address}
            description={description}
        />
    );
}

export async function getStaticPaths() {
    const { client, collection: meetupsCollection } = await getCollection(
        "meetups"
    );

    const meetups = await meetupsCollection.find({}, { _id: 1 }).toArray();

    client.close();

    return {
        fallback: false,
        paths: meetups.map((meetup) => ({
            params: { meetupId: meetup._id.toString() },
        })),
    };
}

export async function getStaticProps(context) {
    const meetupId = context.params.meetupId;

    const { client, collection: meetupsCollection } = await getCollection(
        "meetups"
    );

    const selectedMeetup = await meetupsCollection.findOne({
        _id: ObjectId(meetupId),
    });

    client.close();

    return {
        props: {
            meetupData: {
                id: selectedMeetup._id.toString(),
                title: selectedMeetup.title,
                address: selectedMeetup.address,
                image: selectedMeetup.image,
                description: selectedMeetup.description,
            },
        },
    };
}

export default MeetupDetails;
