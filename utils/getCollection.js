import { MongoClient } from "mongodb";

async function getCollection(documentCollection) {
    const client = await MongoClient.connect(
        `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@api.89mmt.mongodb.net/api?retryWrites=true&w=majority`
    );
    const db = client.db();

    const collection = db.collection(documentCollection);

    return {
        client,
        collection,
    };
}

export default getCollection;
